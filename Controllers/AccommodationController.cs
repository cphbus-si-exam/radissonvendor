using System.IO;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Mvc;
using Radisson.Models;

namespace Radisson.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class AccommodationController : Controller
    {
        [HttpGet]
        public IActionResult Get(){
            return Ok("Radisson accommodation alive");
        }


        [HttpPost]
        public IActionResult Post([FromBody]AccBookingModel ABMdata){

            if(!ModelState.IsValid){
                return BadRequest();
            }
            
            try
            {
                var res = new AccBookingModelRes();
                res.eventid = ABMdata.eventid;
                res.request_type=ABMdata.request_type;
                if (ABMdata.numberofrooms > 100){
                    res.status = false;
                    res.description = "No available rooms, in the specified timeframe";
                }else
                {
                    res.status=true;
                    res.additional_info ="Contactperson: Lulu lemon";
                }
                var SW = new StringWriter();
                var XS = new XmlSerializer(res.GetType());
                XS.Serialize(SW,res);
                return Content(SW.ToString(), "text/xml", System.Text.Encoding.UTF8);

            }
            catch (System.Exception)
            {
                return BadRequest();                
            }
        }
    }
}