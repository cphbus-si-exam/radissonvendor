FROM mcr.microsoft.com/dotnet/core/sdk:2.2 as base
WORKDIR /src

COPY ./Radisson.csproj .
RUN dotnet restore

COPY . .
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2 as final
WORKDIR /app

COPY --from=base /src/out .
ENTRYPOINT [ "dotnet", "Radisson.dll" ]
