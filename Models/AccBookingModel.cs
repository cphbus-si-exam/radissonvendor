using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Radisson.Models
{
    [XmlRoot(ElementName = "accommodationbooking", Namespace = "")]
    public class AccBookingModel
    {
        [Required]
        [XmlElement(DataType = "int", ElementName = "numberofrooms")]
        public int numberofrooms { get; set; }

        [Required]
        [XmlElement(DataType = "string", ElementName = "startdate")]
        public string startdate { get; set; }

        [Required]
        [XmlElement(DataType = "string", ElementName = "enddate")]
        public string enddate { get; set; }   

        [Required]
        [XmlElement(DataType = "string", ElementName = "eventid")]
        public string eventid { get; set; }   

        [Required]
        [XmlElement(DataType = "string", ElementName = "request_type")]
        public string request_type { get; set; }

    }
}