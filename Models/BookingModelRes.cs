using System.Runtime.Serialization;

namespace RadissonVenue
{
    [DataContract]
    public class BookingModelRes
    {
        [DataMember]
        public bool status { get; set; }
        [DataMember]
        public string description { get; set; }

        [DataMember]
        public string eventid { get; set; }

        [DataMember]
        public string additinal_info { get; set; }
        [DataMember]
        public string request_type { get; set; }

    }
}