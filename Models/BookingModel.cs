using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace RadissonVenue
{
    [DataContract]
    public class BookingModel
    {
        [DataMember]
        [Required]
        public int numberofpersons { get; set; }
        [DataMember]
        [Required]
        public string startdate { get; set; }
        [DataMember]
        [Required]
        public string enddate { get; set; }
        [DataMember]
        [Required]
        public string address { get; set; }
        [DataMember]
        [Required]
        public string eventid { get; set; }

        [DataMember]
        [Required]
        public string request_type { get; set; }

    }
}