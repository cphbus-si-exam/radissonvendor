namespace RadissonVenue
{
    public class VenueServiceImpl : IVenueService
    {
        public BookingModelRes RequestBooking(BookingModel bookingData)
        {
            var mods = bookingData.numberofpersons%5;
            var res = new BookingModelRes();
            res.eventid = bookingData.eventid;
            res.request_type = bookingData.request_type;

            if(mods==0){
                res.status = true;
                res.additinal_info = "Denmark, contactperson: Tom Baker";
            }
            if(mods==1){
                res.status = true;
                res.additinal_info = "Sweden, contactperson: Liz Taylor";
            }
            if(mods==2){
                res.status = true;
                res.additinal_info = "Norway, contactperson: Clive Owen";
            }
            if(mods==3){
                res.status = true;
                res.additinal_info = "Finland, contactperson: Axel Rose";
            }
            if(mods==4){
                res.status = false;
                res.description = "no avaiable facilities in the selected period";
            }

            return res;
        }
    }
}