using System.ServiceModel;

namespace RadissonVenue
{
    [ServiceContract]
    public interface IVenueService
    {
        [OperationContract]
        BookingModelRes RequestBooking(BookingModel bookingData);
    }
}